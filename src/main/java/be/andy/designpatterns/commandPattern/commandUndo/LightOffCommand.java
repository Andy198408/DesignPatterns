package be.andy.designpatterns.commandPattern.commandUndo;

import be.andy.designpatterns.commandPattern.model.CommandUndo;
import be.andy.designpatterns.commandPattern.model.Light;

public class LightOffCommand implements CommandUndo {

    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.off();
    }

    @Override
    public void undo() {
        light.on();
    }
}
