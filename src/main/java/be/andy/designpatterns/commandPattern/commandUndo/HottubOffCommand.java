package be.andy.designpatterns.commandPattern.commandUndo;

import be.andy.designpatterns.commandPattern.model.Command;
import be.andy.designpatterns.commandPattern.model.CommandUndo;
import be.andy.designpatterns.commandPattern.model.Hottub;

public class HottubOffCommand implements CommandUndo {
	Hottub hottub;

	public HottubOffCommand(Hottub hottub) {
		this.hottub = hottub;
	}

	public void execute() {
		hottub.cool();
		hottub.off();
	}

	@Override
	public void undo() {
		hottub.on();
		hottub.bubblesOn();
	}
}
