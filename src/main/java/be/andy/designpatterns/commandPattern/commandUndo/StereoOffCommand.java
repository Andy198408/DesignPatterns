package be.andy.designpatterns.commandPattern.commandUndo;

import be.andy.designpatterns.commandPattern.model.Command;
import be.andy.designpatterns.commandPattern.model.CommandUndo;
import be.andy.designpatterns.commandPattern.model.Stereo;

public class StereoOffCommand implements CommandUndo {
	Stereo stereo;
 
	public StereoOffCommand(Stereo stereo) {
		this.stereo = stereo;
	}
 
	public void execute() {
		stereo.off();
	}

	@Override
	public void undo() {
		stereo.on();
	}
}
