package be.andy.designpatterns.commandPattern.commandUndo;

import be.andy.designpatterns.commandPattern.model.CommandUndo;
import be.andy.designpatterns.commandPattern.model.Hottub;

public class HottubOnCommand implements CommandUndo {
    Hottub hottub;

    public HottubOnCommand(Hottub hottub) {
        this.hottub = hottub;
    }

    public void execute() {
        hottub.on();
        hottub.heat();
        hottub.bubblesOn();
    }

    @Override
    public void undo() {
        hottub.cool();
        hottub.jetsOff();
        hottub.bubblesOff();
    }
}
