package be.andy.designpatterns.commandPattern.model;

public class NoCommand implements Command {
	public void execute() { }
}
