package be.andy.designpatterns.commandPattern.model;

public class NoCommandUndo implements CommandUndo {
    public void execute() {
    }

    @Override
    public void undo() {

    }
}
