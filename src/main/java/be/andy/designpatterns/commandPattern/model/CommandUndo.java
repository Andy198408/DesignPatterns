package be.andy.designpatterns.commandPattern.model;

public interface CommandUndo extends Command {
    void undo();

}
