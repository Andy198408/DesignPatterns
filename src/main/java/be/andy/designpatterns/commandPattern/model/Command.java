package be.andy.designpatterns.commandPattern.model;

public interface Command {
    void execute();

}
