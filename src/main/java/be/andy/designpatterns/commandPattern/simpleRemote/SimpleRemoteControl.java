package be.andy.designpatterns.commandPattern.simpleRemote;

import be.andy.designpatterns.commandPattern.model.Command;

public class SimpleRemoteControl {
    Command slot;

    public void setCommand(Command command) {
        slot = command;
    }

    public void buttonWasPressed() {
        slot.execute();
    }

}
