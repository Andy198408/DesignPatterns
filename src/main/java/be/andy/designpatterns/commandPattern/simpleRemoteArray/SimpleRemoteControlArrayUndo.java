package be.andy.designpatterns.commandPattern.simpleRemoteArray;

import be.andy.designpatterns.commandPattern.model.CommandUndo;
import be.andy.designpatterns.commandPattern.model.NoCommandUndo;

public class SimpleRemoteControlArrayUndo {
    CommandUndo[] onCommands;
    CommandUndo[] offCommands;
    CommandUndo undo;

    public SimpleRemoteControlArrayUndo() {
        onCommands = new CommandUndo[7];
        offCommands = new CommandUndo[7];

        NoCommandUndo noCommand = new NoCommandUndo();
        for (int i = 0; i < 7; i++) {
            onCommands[i] = noCommand;
            offCommands[i] = noCommand;
        }
        undo = noCommand;
    }

    public void setCommand(int slot, CommandUndo onCommand, CommandUndo offCommand) {
        onCommands[slot] = onCommand;
        offCommands[slot] = offCommand;
    }

    public void onButtonWasPushed(int slot) {
        onCommands[slot].execute();
        undo = onCommands[slot];
    }

    public void offButtonWasPushed(int slot) {
        offCommands[slot].execute();
        undo = offCommands[slot];
    }

    public void undoButtonWasPushed() {
        undo.undo();
    }

    @Override
    public String toString() {
        StringBuffer stringBuff = new StringBuffer();
        stringBuff.append("\n------ Remote Control -------\n");
        for (int i = 0; i < onCommands.length; i++) {
            stringBuff.append("[slot " + i + "] " + onCommands[i].getClass().getSimpleName()
                    + " " + offCommands[i].getClass().getSimpleName() + "\n"
                    + " " + undo.getClass().getSimpleName() + "\n");
        }
        return stringBuff.toString();
    }
}
