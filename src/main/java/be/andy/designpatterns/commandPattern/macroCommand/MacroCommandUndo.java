package be.andy.designpatterns.commandPattern.macroCommand;

import be.andy.designpatterns.commandPattern.model.CommandUndo;

public class MacroCommandUndo implements CommandUndo {

    CommandUndo[] commands;

    public MacroCommandUndo(CommandUndo[] commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        for (int i = 0; i < commands.length; i++) {
            commands[i].execute();
        }
    }

    @Override
    public void undo() {
        for (int i = 0; i < commands.length; i++) {
            commands[i].undo();
        }
    }
}
