package be.andy.designpatterns.commandPattern.command;

import be.andy.designpatterns.commandPattern.model.Command;
import be.andy.designpatterns.commandPattern.model.Stereo;

public class StereoOffCommand implements Command {
	Stereo stereo;
 
	public StereoOffCommand(Stereo stereo) {
		this.stereo = stereo;
	}
 
	public void execute() {
		stereo.off();
	}
}
