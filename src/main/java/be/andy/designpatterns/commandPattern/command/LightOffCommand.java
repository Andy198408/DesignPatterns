package be.andy.designpatterns.commandPattern.command;

import be.andy.designpatterns.commandPattern.model.Command;
import be.andy.designpatterns.commandPattern.model.Light;

public class LightOffCommand implements Command {

    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.off();
    }
}
