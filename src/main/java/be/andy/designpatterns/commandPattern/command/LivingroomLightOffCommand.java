package be.andy.designpatterns.commandPattern.command;

import be.andy.designpatterns.commandPattern.model.Command;
import be.andy.designpatterns.commandPattern.model.Light;

public class LivingroomLightOffCommand implements Command {
	Light light;

	public LivingroomLightOffCommand(Light light) {
		this.light = light;
	}

	public void execute() {
		light.off();
	}
}
