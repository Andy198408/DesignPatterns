package be.andy.designpatterns.commandPattern.command;

import be.andy.designpatterns.commandPattern.model.CeilingFan;
import be.andy.designpatterns.commandPattern.model.Command;

public class CeilingFanOffCommand implements Command {
	CeilingFan ceilingFan;

	public CeilingFanOffCommand(CeilingFan ceilingFan) {
		this.ceilingFan = ceilingFan;
	}
	public void execute() {
		ceilingFan.off();
	}
}
