package be.andy.designpatterns.adapterPattern;

import java.util.Random;
import java.util.random.RandomGenerator;

public class DuckAdapter implements Turkey {
    Duck duck;

    public DuckAdapter(Duck duck) {
        this.duck = duck;
    }

    @Override
    public void gobble() {
        duck.quack();
    }

    @Override
    public void fly() {
        RandomGenerator rand = new Random();
        if (rand.nextInt(5) == 0) {
            duck.fly();
        }
    }
}
