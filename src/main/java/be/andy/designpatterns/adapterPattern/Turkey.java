package be.andy.designpatterns.adapterPattern;

public interface Turkey {
    void gobble();

    void fly();
}
