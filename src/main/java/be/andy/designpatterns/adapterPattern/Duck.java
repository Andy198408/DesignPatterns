package be.andy.designpatterns.adapterPattern;

public interface Duck {
    void quack();

    void fly();
}
