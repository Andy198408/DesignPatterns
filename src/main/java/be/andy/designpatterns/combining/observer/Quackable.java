package be.andy.designpatterns.combining.observer;

public interface Quackable extends QuackObservable {
	public void quack();
}
