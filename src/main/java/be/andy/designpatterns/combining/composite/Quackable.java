package be.andy.designpatterns.combining.composite;

public interface Quackable {
	public void quack();
}
