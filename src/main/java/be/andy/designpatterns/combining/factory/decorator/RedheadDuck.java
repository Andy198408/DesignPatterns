package be.andy.designpatterns.combining.factory.decorator;

public class RedheadDuck implements Quackable {
	public void quack() {
		System.out.println("Quack");
	}
}
