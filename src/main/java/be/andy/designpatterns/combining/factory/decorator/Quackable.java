package be.andy.designpatterns.combining.factory.decorator;

public interface Quackable {
	public void quack();
}
