package be.andy.designpatterns.combining.factory;

public interface Quackable {
	public void quack();
}
