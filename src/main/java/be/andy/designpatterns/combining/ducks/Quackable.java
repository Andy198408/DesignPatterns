package be.andy.designpatterns.combining.ducks;

public interface Quackable {
	public void quack();
}
