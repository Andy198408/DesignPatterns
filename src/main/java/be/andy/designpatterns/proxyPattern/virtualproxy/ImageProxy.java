package be.andy.designpatterns.proxyPattern.virtualproxy;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

class ImageProxy implements Icon {
    public static final String DEFAULTCOVERLOCATION = "C:\\Users\\andyv\\Pictures\\Saved Pictures\\141588-4k-resolution-wallpaper-top-free-4k-resolution-background.jpg";
    public static final String DEFAULT_COVER_DESCRIPTION = "Default Cover";
    volatile ImageIcon imageIcon;
    final URL imageURL;
    Thread retrievalThread;
    boolean retrieving = false;

    public ImageProxy(URL url) {
        imageURL = url;
    }

    public int getIconWidth() {
        if (imageIcon != null) {
            return imageIcon.getIconWidth();
        } else {
            return 800;
        }
    }

    public int getIconHeight() {
        if (imageIcon != null) {
            return imageIcon.getIconHeight();
        } else {
            return 600;
        }
    }

    synchronized void setImageIcon(ImageIcon imageIcon) {
        this.imageIcon = imageIcon;
    }

    public void paintIcon(final Component c, Graphics g, int x, int y) {
        if (imageIcon != null) {
            imageIcon.paintIcon(c, g, x, y);
        } else {
            g.drawString("Loading album cover, please wait...", x + 300, y + 190);
            if (!retrieving) {
                retrieving = true;

//				retrievalThread = new Thread(new Runnable() {
//					public void run() {
//						try {
//							setImageIcon(new ImageIcon(imageURL, "Album Cover"));
//							c.repaint();
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				});

                retrievalThread = new Thread(() -> {
                    try {
                        ImageIcon albumCover = new ImageIcon(imageURL, "Album Cover");
                        if (albumCover.getImageLoadStatus() == 4) {
                            albumCover = new ImageIcon(DEFAULTCOVERLOCATION, DEFAULT_COVER_DESCRIPTION);
                        }
                        setImageIcon(albumCover);
                        c.repaint();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                retrievalThread.start();

            }
        }
    }
}
