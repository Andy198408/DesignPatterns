package be.andy.designpatterns.proxyPattern.gumball;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

//  Future reference the arguments are passed through run configurations.
public class GumballMachineTestDrive {

    public static void main(String[] args) {
//        GumballMachineRemote gumballMachine;
        int count;


        if (args.length < 2) {
            System.out.println("GumballMachine <name> <inventory>");
            System.exit(1);
        }

        try {
            count = Integer.parseInt(args[1]);

            GumballMachineRemote gumballMachine1 =
                    new GumballMachine("Gumball1", 150);
            GumballMachineRemote gumballMachine2 =
                    new GumballMachine("Gumball2", 200);
            GumballMachineRemote gumballMachine3 =
                    new GumballMachine("Gumball3", 300);

//			Naming.rebind("//" + args[0] + "/gumballmachine", gumballMachine);

            Registry registry = LocateRegistry.createRegistry(301);
            registry.rebind("Gumball1", gumballMachine1);
            registry.rebind("Gumball2", gumballMachine2);
            registry.rebind("Gumball3", gumballMachine3);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
