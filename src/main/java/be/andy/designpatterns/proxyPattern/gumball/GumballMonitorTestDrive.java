package be.andy.designpatterns.proxyPattern.gumball;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GumballMonitorTestDrive {

    public static void main(String[] args) {
//		String[] location = {"rmi://santafe.mightygumball.com/gumballmachine",
//		                     "rmi://boulder.mightygumball.com/gumballmachine",
//		                     "rmi://austin.mightygumball.com/gumballmachine"};

//		if (args.length >= 0)
//        {
//            location = new String[1];
//            location[0] = "rmi://" + args[0] + "/gumballmachine";
//        }


//		GumballMonitor[] monitor = new GumballMonitor[location.length];


//        for (int i = 0; i < location.length; i++) {
        List<GumballMonitor> gumballMonitors = new ArrayList<>();
        try {
            Registry registry = LocateRegistry.getRegistry(301);
            GumballMachineRemote gumball1 = (GumballMachineRemote) registry.lookup("Gumball1");
            GumballMachineRemote gumball2 = (GumballMachineRemote) registry.lookup("Gumball2");
            GumballMachineRemote gumball3 = (GumballMachineRemote) registry.lookup("Gumball3");
            List<GumballMachineRemote> gumballMachineRemotes = Arrays.asList(gumball1, gumball2, gumball3);
//                GumballMachineRemote machine =
//                        (GumballMachineRemote) Naming.lookup(location[i]);
            for (GumballMachineRemote gumballMachineRemote : gumballMachineRemotes) {
                GumballMonitor monitor = new GumballMonitor(gumballMachineRemote);
                System.out.println(monitor);
                System.out.println(monitor.machine.toString());
                gumballMonitors.add(monitor);
            }
//                monitor[i] = new GumballMonitor(machine);
//                System.out.println(monitor[i]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        gumballMonitors.forEach(GumballMonitor::report);
//        }

//        for (int i = 0; i < monitor.length; i++) {
//            monitor[i].report();
//        }
    }
}
