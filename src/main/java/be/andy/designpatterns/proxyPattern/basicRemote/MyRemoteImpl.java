package be.andy.designpatterns.proxyPattern.basicRemote;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyRemoteImpl extends UnicastRemoteObject implements MyRemote {
    private static final long serialVersionUID = 1L;
    private static Registry registry;

    public MyRemoteImpl() throws RemoteException {

    }

    public static void main(String[] args) {
        try {
            registry = LocateRegistry.createRegistry(300);


            MyRemote service = new MyRemoteImpl();
//            Naming.rebind("RemoteHello", service);
            registry.rebind("RemoteHello", service);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public String sayHello() throws RemoteException {
        Logger myRemoteImpl = Logger.getLogger("MyRemoteImpl");
        myRemoteImpl.log(Level.ALL, "Used");
        System.out.println("Wggggatt");
        return "Serverside says, 'Hello'";
    }
}
