package be.andy.designpatterns.proxyPattern.basicRemote;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class MyRemoteClient {
    public static void main(String[] args) {
        new MyRemoteClient().go();
    }

    public void go() {
        try {
//            MyRemote service = (MyRemote) Naming.lookup("rmi://127.0.0.1/RemoteHello");
//                                                                    hostname or IP where service is running!
            // easier to use in dev environment
            Registry registry = LocateRegistry.getRegistry(300);
            MyRemote remoteHello = (MyRemote) registry.lookup("RemoteHello"); // stub/proxy
            String s = remoteHello.sayHello();
            System.out.println(s);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}


