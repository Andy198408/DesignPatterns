package be.andy.designpatterns.proxyPattern.basicRemote;

import java.rmi.Remote;
import java.rmi.RemoteException;

interface MyRemote extends Remote {
    String sayHello() throws RemoteException;
}
