package be.andy.designpatterns.StrategyPattern.Model;

import be.andy.designpatterns.StrategyPattern.Implementations.FlyWithWings;
import be.andy.designpatterns.StrategyPattern.Implementations.Quack;

public class MallardDuck extends Duck {

    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }
    @Override
    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
