package be.andy.designpatterns.StrategyPattern.Model;

import be.andy.designpatterns.StrategyPattern.Implementations.FlyNoWay;
import be.andy.designpatterns.StrategyPattern.Implementations.Quack;

public class ModelDuck extends Duck {
    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }
    @Override
    public void display() {
        System.out.println("I'm a model duck");
    }
}
