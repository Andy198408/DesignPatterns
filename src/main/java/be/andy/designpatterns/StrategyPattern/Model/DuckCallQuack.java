package be.andy.designpatterns.StrategyPattern.Model;

import be.andy.designpatterns.StrategyPattern.Implementations.Quack;

public class DuckCallQuack extends Quack {
    @Override
    public void quack() {
        super.quack();
    }
}
