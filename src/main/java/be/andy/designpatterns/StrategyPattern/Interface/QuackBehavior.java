package be.andy.designpatterns.StrategyPattern.Interface;

public interface QuackBehavior {
    void quack();
}
