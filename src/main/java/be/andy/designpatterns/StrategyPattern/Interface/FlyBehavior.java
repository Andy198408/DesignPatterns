package be.andy.designpatterns.StrategyPattern.Interface;

public interface FlyBehavior {
    void fly();
}
