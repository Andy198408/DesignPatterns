package be.andy.designpatterns.StrategyPattern.main;

import be.andy.designpatterns.StrategyPattern.Implementations.FlyRocketPowered;
import be.andy.designpatterns.StrategyPattern.Model.Duck;
import be.andy.designpatterns.StrategyPattern.Model.DuckCallQuack;
import be.andy.designpatterns.StrategyPattern.Model.MallardDuck;
import be.andy.designpatterns.StrategyPattern.Model.ModelDuck;

public class MiniDuckSimulator {
    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();

        DuckCallQuack duckCallQuack = new DuckCallQuack();
        duckCallQuack.quack();
    }
}
