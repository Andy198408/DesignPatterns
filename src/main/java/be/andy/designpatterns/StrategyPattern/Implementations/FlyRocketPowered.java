package be.andy.designpatterns.StrategyPattern.Implementations;

import be.andy.designpatterns.StrategyPattern.Interface.FlyBehavior;

public class FlyRocketPowered implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I'm flying with a rocket!");
    }
}
