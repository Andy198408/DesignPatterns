package be.andy.designpatterns.StrategyPattern.Implementations;

import be.andy.designpatterns.StrategyPattern.Interface.QuackBehavior;

public class Squeak implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Squeak");
    }
}
