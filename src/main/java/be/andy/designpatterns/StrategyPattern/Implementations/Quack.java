package be.andy.designpatterns.StrategyPattern.Implementations;

import be.andy.designpatterns.StrategyPattern.Interface.QuackBehavior;

public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Quack");
    }
}
