package be.andy.designpatterns.StrategyPattern.Implementations;

import be.andy.designpatterns.StrategyPattern.Interface.FlyBehavior;

public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I'm flying!");
    }
}
