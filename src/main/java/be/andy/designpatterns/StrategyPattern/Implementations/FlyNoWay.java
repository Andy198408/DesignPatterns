package be.andy.designpatterns.StrategyPattern.Implementations;

import be.andy.designpatterns.StrategyPattern.Interface.FlyBehavior;

public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I can't fly");
    }
}
