package be.andy.designpatterns.DecoratorPattern.beverage;

import java.util.ArrayList;
import java.util.List;

public abstract class Beverage {

    List<Beverage> condimentList = new ArrayList<>();

    public enum Size {TALL, GRANDE, VENTI}

    ;
    Size size = Size.TALL;
    String description = "Unknown Beverage";

    public String getDescription() {
        return description;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Size getSize() {
        return this.size;
    }

    public abstract double cost();

    public void printList() {
        condimentList.forEach(p -> System.out.println(p.getDescription()));
    }

    public List<Beverage> getCondimentList() {
        return condimentList;
    }
}
