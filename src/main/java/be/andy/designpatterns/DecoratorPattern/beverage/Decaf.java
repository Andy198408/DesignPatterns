package be.andy.designpatterns.DecoratorPattern.beverage;

public class Decaf extends Beverage {
    public Decaf() {
        description = "Decaf Coffee";
        condimentList.add(this);
    }

    @Override
    public double cost() {
        return 1.05;
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }
}
