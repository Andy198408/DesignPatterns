package be.andy.designpatterns.DecoratorPattern.beverage;

public class Espresso extends Beverage {

    public Espresso() {
        description = "Espresso";
        condimentList.add(this);
    }

    public double cost() {
        return 1.99;
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }
}
