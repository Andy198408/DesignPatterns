package be.andy.designpatterns.DecoratorPattern.beverage;

public class DarkRoast extends Beverage {
    public DarkRoast() {
        description = "DarkRoast Coffee";
        condimentList.add(this);
    }

    @Override
    public double cost() {
        return .99;
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }

}
