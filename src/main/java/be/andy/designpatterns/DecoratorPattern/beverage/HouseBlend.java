package be.andy.designpatterns.DecoratorPattern.beverage;

public class HouseBlend extends Beverage {
    public HouseBlend() {
        description = "House Blend Coffee";
        condimentList.add(this);
    }

    public double cost() {
        return .89;
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }
}



