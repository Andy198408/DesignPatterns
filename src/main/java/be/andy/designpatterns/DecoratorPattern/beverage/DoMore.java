package be.andy.designpatterns.DecoratorPattern.beverage;

public interface DoMore {
    String getDescription();

    void doMoreStuff();
}
