package be.andy.designpatterns.DecoratorPattern.beverage;

public class Soy extends CondimentDecorator {

    public Soy(Beverage beverage) {
        this.beverage = beverage;
        condimentList.add(this);
        this.size = beverage.size;
    }

    @Override
    public double cost() {
        Size size = this.getSize();
        switch (size) {
            case TALL -> {
                return beverage.cost() + .10;
            }
            case GRANDE -> {
                return beverage.cost() + .15;
            }
            case VENTI -> {
                return beverage.cost() + .20;
            }
            default -> {
                return beverage.cost() + .30;
            }
        }
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Soy";
    }
}
