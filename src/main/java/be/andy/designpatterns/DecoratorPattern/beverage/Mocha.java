package be.andy.designpatterns.DecoratorPattern.beverage;

public class Mocha extends CondimentDecorator implements DoMore {

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
        condimentList.add(this);
        this.size = beverage.size;
    }

    @Override
    public double cost() {
        return beverage.cost() + .20;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Mocha";
    }


    @Override
    public void doMoreStuff() {
        System.out.println("Do more stuff!!");
    }
}
