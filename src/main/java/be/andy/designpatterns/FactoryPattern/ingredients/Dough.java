package be.andy.designpatterns.FactoryPattern.ingredients;

public interface Dough {
	public String toString();
}
