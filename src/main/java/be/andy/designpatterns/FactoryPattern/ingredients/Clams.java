package be.andy.designpatterns.FactoryPattern.ingredients;

public interface Clams {
	public String toString();
}
