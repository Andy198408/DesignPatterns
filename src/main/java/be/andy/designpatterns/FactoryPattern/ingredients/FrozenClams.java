package be.andy.designpatterns.FactoryPattern.ingredients;

public class FrozenClams implements Clams {

	public String toString() {
		return "Frozen Clams from Chesapeake Bay";
	}
}
