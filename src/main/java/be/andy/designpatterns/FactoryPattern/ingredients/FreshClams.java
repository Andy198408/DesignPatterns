package be.andy.designpatterns.FactoryPattern.ingredients;

public class FreshClams implements Clams {

	public String toString() {
		return "Fresh Clams from Long Island Sound";
	}
}
