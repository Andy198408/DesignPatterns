package be.andy.designpatterns.FactoryPattern.ingredients;

public interface Sauce {
	public String toString();
}
