package be.andy.designpatterns.FactoryPattern.ingredients;

public class MozzarellaCheese implements Cheese {

	public String toString() {
		return "Shredded Mozzarella";
	}
}
