package be.andy.designpatterns.FactoryPattern.ingredients;

public class BlackOlives implements Veggies {

	public String toString() {
		return "Black Olives";
	}
}
