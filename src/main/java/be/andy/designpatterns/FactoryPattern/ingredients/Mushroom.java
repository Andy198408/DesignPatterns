package be.andy.designpatterns.FactoryPattern.ingredients;

public class Mushroom implements Veggies {

	public String toString() {
		return "Mushrooms";
	}
}
