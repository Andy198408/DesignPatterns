package be.andy.designpatterns.FactoryPattern.ingredients;

public class ThinCrustDough implements Dough {
	public String toString() {
		return "Thin Crust Dough";
	}
}
