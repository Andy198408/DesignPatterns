package be.andy.designpatterns.FactoryPattern.ingredients;

public class MarinaraSauce implements Sauce {
	public String toString() {
		return "Marinara Sauce";
	}
}
