package be.andy.designpatterns.FactoryPattern.ingredients;

public class Eggplant implements Veggies {

	public String toString() {
		return "Eggplant";
	}
}
