package be.andy.designpatterns.FactoryPattern.ingredients;

public interface Veggies {
	public String toString();
}
