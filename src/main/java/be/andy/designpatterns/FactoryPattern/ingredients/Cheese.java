package be.andy.designpatterns.FactoryPattern.ingredients;

public interface Cheese {
	public String toString();
}
