package be.andy.designpatterns.FactoryPattern.ingredients;

public class SlicedPepperoni implements Pepperoni {

	public String toString() {
		return "Sliced Pepperoni";
	}
}
