package be.andy.designpatterns.FactoryPattern.ingredients;

public class Garlic implements Veggies {

	public String toString() {
		return "Garlic";
	}
}
