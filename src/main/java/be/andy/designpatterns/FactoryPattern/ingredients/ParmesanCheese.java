package be.andy.designpatterns.FactoryPattern.ingredients;

public class ParmesanCheese implements Cheese {

	public String toString() {
		return "Shredded Parmesan";
	}
}
