package be.andy.designpatterns.FactoryPattern.ingredients;

public interface Pepperoni {
	public String toString();
}
