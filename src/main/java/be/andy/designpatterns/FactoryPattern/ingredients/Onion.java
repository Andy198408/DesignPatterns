package be.andy.designpatterns.FactoryPattern.ingredients;

public class Onion implements Veggies {

	public String toString() {
		return "Onion";
	}
}
