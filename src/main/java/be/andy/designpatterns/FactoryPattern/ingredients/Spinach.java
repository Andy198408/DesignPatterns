package be.andy.designpatterns.FactoryPattern.ingredients;

public class Spinach implements Veggies {

	public String toString() {
		return "Spinach";
	}
}
