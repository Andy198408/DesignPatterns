package be.andy.designpatterns.FactoryPattern.ingredients;

public class ThickCrustDough implements Dough {
	public String toString() {
		return "ThickCrust style extra thick crust dough";
	}
}
