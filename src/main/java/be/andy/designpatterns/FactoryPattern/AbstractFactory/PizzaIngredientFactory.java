package be.andy.designpatterns.FactoryPattern.AbstractFactory;

import be.andy.designpatterns.FactoryPattern.ingredients.*;
public interface PizzaIngredientFactory {
        public Dough createDough();
        public Sauce createSauce();
        public Cheese createCheese();
        public Veggies[] createVeggies();
        public Pepperoni createPepperoni();
        public Clams createClam();

}
