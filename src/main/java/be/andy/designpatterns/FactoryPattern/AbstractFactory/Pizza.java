package be.andy.designpatterns.FactoryPattern.AbstractFactory;

import be.andy.designpatterns.FactoryPattern.ingredients.*;
import lombok.Setter;

public abstract class Pizza {
    @Setter

    String name;
    Dough dough;
    Sauce sauce;
    Veggies veggies[];
    Cheese cheese;
    Pepperoni pepperoni;
    Clams clam;

    abstract void prepare();

    void bake() {
        System.out.println("Bake for 25 minutes at 350");
    }

    void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }

    public String getName() {
        return name;
    }
}
