package be.andy.designpatterns.FactoryPattern.AbstractFactory;

public abstract class PizzaStore {

    // this is the factory method, subclass will implement this
    // and instantiate new classes!
    public abstract Pizza createPizza(String type);

    // Orderpizza will use the createPizza method implemented by the subclass.
    // So the type of store can create the type of pizza defined in the
    // subclass.
    public Pizza orderPizza(String item) {
        Pizza pizza;
        pizza = createPizza(item);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
