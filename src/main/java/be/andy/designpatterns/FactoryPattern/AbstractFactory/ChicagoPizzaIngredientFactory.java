package be.andy.designpatterns.FactoryPattern.AbstractFactory;


import be.andy.designpatterns.FactoryPattern.ingredients.*;

import java.util.Arrays;

public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new ThickCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PlumTomatoSauce();
    }

    @Override
    public Cheese createCheese() {
        return new MozzarellaCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        return Arrays.asList(new Spinach(), new Eggplant(), new BlackOlives()).toArray(Veggies[]::new);
    }

    @Override
    public Pepperoni createPepperoni() {
        return new SlicedPepperoni();
    }

    @Override
    public Clams createClam() {
        return new FrozenClams();
    }
}
