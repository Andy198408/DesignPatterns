package be.andy.designpatterns.singletonPattern;

public enum EnumSingleton {
    // Java program to demonstrate the example
    // of using Enum as Singleton
    INSTANCE;
    int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


}

enum MyEnumSingleton {
    INSTANCE;

    private String data;

    MyEnumSingleton() {
        data = "Hello, Singleton!";
    }

    public String getData() {
        return data;
    }
}

class Main {

    public static void main(String[] args) {
        EnumSingleton singleton = EnumSingleton.INSTANCE;

        System.out.println(singleton.getValue());
        singleton.setValue(2);
        System.out.println(singleton.getValue());

        // Access the singleton instance
        MyEnumSingleton mySingleton = MyEnumSingleton.INSTANCE;

        // Access data and perform operations
        String data = mySingleton.getData();
        System.out.println(data);
    }
}

/*
* The enum singleton pattern in Java has several pros and cons:

Pros:

Simplicity: Implementing a singleton using an enum is straightforward and concise. It doesn't require explicit synchronization or complex code.
Thread Safety: Enum singletons are inherently thread-safe. Java ensures that enum values are initialized only once, making it safe for concurrent access.
Serialization: Enum singletons provide built-in support for serialization without any extra effort. The deserialized object will always refer to the same enum instance.
Robustness against Reflection: Enum singletons are robust against most forms of reflection-based attacks, as the enum type cannot be instantiated using reflection.
*
*
Cons:

Limited Extensibility: Enum singletons cannot be subclassed, which can be a limitation if you need to extend or modify the singleton behavior in the future.
Reduced Flexibility: Enum singletons have a fixed set of instances defined by the enum constants. It's not possible to create additional instances dynamically.
Limited Dependency Injection: Enum singletons do not work well with dependency injection frameworks that require creating and managing singleton instances through their container.
Difficulty in Lazy Initialization: Enum singletons are eagerly initialized when the enum class is loaded, which means they don't support lazy initialization.
* If the singleton's initialization requires heavy resources or is time-consuming, it may impact the application's startup time.
It's important to consider these factors when deciding whether to use the enum singleton pattern based on your specific requirements and design considerations.
* */