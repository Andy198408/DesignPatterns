package be.andy.designpatterns.singletonPattern;

public class MySingletonSynchronizedThreadSafe {
    private static MySingletonSynchronizedThreadSafe mySingleton;

    private MySingletonSynchronizedThreadSafe() {
    }

    public static synchronized MySingletonSynchronizedThreadSafe getInstance() {
        if (mySingleton == null) {
            mySingleton = new MySingletonSynchronizedThreadSafe();
        }
        return mySingleton;
    }

}
