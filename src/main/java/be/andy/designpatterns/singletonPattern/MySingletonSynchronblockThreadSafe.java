package be.andy.designpatterns.singletonPattern;

public class MySingletonSynchronblockThreadSafe {
    private volatile static MySingletonSynchronblockThreadSafe mySingleton;

    private MySingletonSynchronblockThreadSafe() {
    }

    public static MySingletonSynchronblockThreadSafe getInstance() {
        if (mySingleton == null) {
            synchronized (MySingletonSynchronblockThreadSafe.class) {
                if (mySingleton == null) {
                    mySingleton = new MySingletonSynchronblockThreadSafe();
                }
            }
        }
        return mySingleton;
    }

}
