package be.andy.designpatterns.singletonPattern;

public class MySingletonEagerlyThreadSafe {
    private static MySingletonEagerlyThreadSafe uniqueInstance = new MySingletonEagerlyThreadSafe();

    private MySingletonEagerlyThreadSafe() {
        //no instance
    }

    public static MySingletonEagerlyThreadSafe getInstance() {
        return uniqueInstance;
    }

}
