package be.andy.designpatterns.iteratorPattern.implicit;

import java.util.Iterator;

public interface Menu {
	public Iterator<MenuItem> createIterator();
}
