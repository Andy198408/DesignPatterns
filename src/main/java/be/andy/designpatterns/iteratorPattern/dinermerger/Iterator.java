package be.andy.designpatterns.iteratorPattern.dinermerger;

public interface Iterator {
	boolean hasNext();
	MenuItem next();
}
