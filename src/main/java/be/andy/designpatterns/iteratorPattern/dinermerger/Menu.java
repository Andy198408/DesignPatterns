package be.andy.designpatterns.iteratorPattern.dinermerger;

public interface Menu {
	public Iterator createIterator();
}
