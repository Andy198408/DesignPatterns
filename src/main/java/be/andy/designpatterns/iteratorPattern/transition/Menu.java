package be.andy.designpatterns.iteratorPattern.transition;

import java.util.Iterator;

public interface Menu {
	public Iterator<?> createIterator();
}
