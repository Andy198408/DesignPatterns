package be.andy.designpatterns.iteratorPattern.transition;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class PancakeHouseMenuIterator implements Iterator<MenuItem> {

    ArrayList<MenuItem> list;
    int position = 0;

    public PancakeHouseMenuIterator(ArrayList<MenuItem> list) {
        this.list = list;
    }

    @Override
    public void remove() {
        if (position <= 0) {
            throw new IllegalStateException
                    ("You can't remove an item until you've done at least one next()");
        }
        if (list.get(position - 1) != null) {

        }
    }

    @Override
    public void forEachRemaining(Consumer<? super MenuItem> action) {
        Iterator.super.forEachRemaining(action);
    }

    @Override
    public boolean hasNext() {
        if (list.size() >= position || list.get(position) == null) {
            return false;
        }
        return true;
    }

    @Override
    public MenuItem next() {
        return list.get(position++);
    }
}
