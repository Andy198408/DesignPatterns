package be.andy.designpatterns.templateMethodPattern;

public class Tea extends CaffeineBeverage {

    @Override
    void brew() {
        System.out.println("Steeping the tea");
    }

    @Override
    void addCondiments() {
        System.out.println("Adding Lemon");
    }

    @Override
    void boilWater() {
        System.out.println("Tea Boils 3 min!");
    }

}
