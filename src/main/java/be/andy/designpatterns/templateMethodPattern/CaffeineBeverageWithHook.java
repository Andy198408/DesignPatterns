package be.andy.designpatterns.templateMethodPattern;

import java.util.Objects;

public abstract class CaffeineBeverageWithHook implements Comparable<CaffeineBeverageWithHook> {
    public String test;

    final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()) {
            addCondiments();
        }
    }

    boolean customerWantsCondiments() {
        return true;
    }

    abstract void brew();

    abstract void addCondiments();

    void boilWater() {
        System.out.println("Boiling water");
    }

    void pourInCup() {


        System.out.println("Pouring into cup");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaffeineBeverageWithHook that = (CaffeineBeverageWithHook) o;
        return test.equals(that.test);
    }

    @Override
    public int hashCode() {
        return Objects.hash(test);
    }

    @Override
    public int compareTo(CaffeineBeverageWithHook o) {
        return this.test.compareTo(o.test);
    }

    @Override
    public String toString() {
        return "CaffeineBeverageWithHook{" +
                "test='" + test + '\'' +
                '}';
    }
}
