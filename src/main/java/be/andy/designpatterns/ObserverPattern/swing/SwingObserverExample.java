package be.andy.designpatterns.ObserverPattern.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingObserverExample {
    JFrame frame;

    public static void main(String[] args) {
        SwingObserverExample example = new SwingObserverExample();
        example.go();
    }

    public void go() {
        frame = new JFrame();
        JButton button = new JButton("Should I do it?");
//        button.addActionListener(new AngelListener());
//        button.addActionListener(new AngelListener());
        button.addActionListener(e -> System.out.println("Don't do it you might regret it!"));
        button.addActionListener(e -> System.out.println("Come on, do it!"));
        button.addActionListener(e -> {
            if (Math.random() * 10 > 5) {
                System.out.println("High");
            } else {
                System.out.println("Low");
            }
        });
        frame.add(button);

        // Set frame properties
        frame.getContentPane().add(BorderLayout.CENTER, button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(BorderLayout.CENTER, button);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    class AngelListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            System.out.println("Don't do it, you might regret it!");
        }
    }

    class DevilListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            System.out.println("Come on, do it!");
        }
    }
}
