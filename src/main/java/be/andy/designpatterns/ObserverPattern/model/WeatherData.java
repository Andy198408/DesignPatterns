package be.andy.designpatterns.ObserverPattern.model;


import be.andy.designpatterns.ObserverPattern.Interface.Observer;
import be.andy.designpatterns.ObserverPattern.Interface.Subject;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class WeatherData implements Subject {
    private List<Observer> observers;

    private static WeatherData weatherData;
    private float temperature;
    private float humidity;
    private float pressure;

    private WeatherData() {
        this.observers = new ArrayList<>();
    }

    public static WeatherData getInstance() {
        if (weatherData == null) {
            weatherData = new WeatherData();
        }
        return weatherData;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
//        for (Observer observer : observers) {
//            observer.update();
//        }
        observers.forEach(Observer::update);
    }

    public void measurementsChanged() {
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }
}
