package be.andy.designpatterns.ObserverPattern.Interface;

public interface Observer {
    public void update();
}
