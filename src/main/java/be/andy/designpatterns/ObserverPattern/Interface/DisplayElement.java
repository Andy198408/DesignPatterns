package be.andy.designpatterns.ObserverPattern.Interface;

public interface DisplayElement {
    public void display();
}
