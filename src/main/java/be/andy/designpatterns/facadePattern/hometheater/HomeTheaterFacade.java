package be.andy.designpatterns.facadePattern.hometheater;

public interface HomeTheaterFacade {
    void watchMovie(String movie);

    void endMovie();

    void listenToRadio(double frequency);

    void endRadio();
}
