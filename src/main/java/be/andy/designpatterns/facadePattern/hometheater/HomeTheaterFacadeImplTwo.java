package be.andy.designpatterns.facadePattern.hometheater;

public class HomeTheaterFacadeImplTwo implements HomeTheaterFacade {



    @Override
    public void watchMovie(String movie) {

    }

    @Override
    public void endMovie() {

    }

    @Override
    public void listenToRadio(double frequency) {

    }

    @Override
    public void endRadio() {

    }
}
