package be.andy.designpatterns.sourceDesignpatterns.combining.decorator;

public class RedheadDuck implements Quackable {
	public void quack() {
		System.out.println("Quack");
	}
}
