package be.andy.designpatterns.sourceDesignpatterns.combining.adapter;

public class RedheadDuck implements Quackable {
	public void quack() {
		System.out.println("Quack");
	}
}
