package be.andy.designpatterns.sourceDesignpatterns.combining.factory;

public interface Quackable {
	public void quack();
}
