package be.andy.designpatterns.sourceDesignpatterns.combining.observer;

public interface Observer {
	public void update(QuackObservable duck);
}
