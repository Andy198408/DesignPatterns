package be.andy.designpatterns.sourceDesignpatterns.combining.adapter;

public class MallardDuck implements Quackable {
	public void quack() {
		System.out.println("Quack");
	}
}
