package be.andy.designpatterns.sourceDesignpatterns.combining.factory;

public class RedheadDuck implements Quackable {
	public void quack() {
		System.out.println("Quack");
	}
}
