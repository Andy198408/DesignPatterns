package be.andy.designpatterns.sourceDesignpatterns.combining.ducks;

public class RubberDuck implements Quackable {
	public void quack() {
		System.out.println("Squeak");
	}
}
