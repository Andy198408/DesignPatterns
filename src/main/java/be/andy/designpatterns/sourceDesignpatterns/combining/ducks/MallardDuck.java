package be.andy.designpatterns.sourceDesignpatterns.combining.ducks;

public class MallardDuck implements Quackable {
	public void quack() {
		System.out.println("Quack");
	}
}
