package be.andy.designpatterns.sourceDesignpatterns.combining.adapter;

public interface Quackable {
	public void quack();
}
