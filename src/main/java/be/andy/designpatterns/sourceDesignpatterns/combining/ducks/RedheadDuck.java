package be.andy.designpatterns.sourceDesignpatterns.combining.ducks;

public class RedheadDuck implements Quackable {
	public void quack() {
		System.out.println("Quack");
	}
}
