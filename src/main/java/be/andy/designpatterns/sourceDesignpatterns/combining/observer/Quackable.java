package be.andy.designpatterns.sourceDesignpatterns.combining.observer;

public interface Quackable extends QuackObservable {
	public void quack();
}
