package be.andy.designpatterns.sourceDesignpatterns.combining.decorator;

public interface Quackable {
	public void quack();
}
