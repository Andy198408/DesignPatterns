package be.andy.designpatterns.sourceDesignpatterns.combining.factory;

public class Goose {
	public void honk() {
		System.out.println("Honk");
	}

	public String toString() {
		return "Goose";
	}
}
