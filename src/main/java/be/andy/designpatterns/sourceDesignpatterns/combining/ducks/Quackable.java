package be.andy.designpatterns.sourceDesignpatterns.combining.ducks;

public interface Quackable {
	public void quack();
}
