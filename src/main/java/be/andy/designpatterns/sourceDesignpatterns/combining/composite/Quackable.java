package be.andy.designpatterns.sourceDesignpatterns.combining.composite;

public interface Quackable {
	public void quack();
}
