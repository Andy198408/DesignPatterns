package be.andy.designpatterns.sourceDesignpatterns.combined.djview;
  
public interface BPMObserver {
	void updateBPM();
}
