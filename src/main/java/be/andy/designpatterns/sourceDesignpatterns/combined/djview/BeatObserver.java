package be.andy.designpatterns.sourceDesignpatterns.combined.djview;
  
public interface BeatObserver {
	void updateBeat();
}
