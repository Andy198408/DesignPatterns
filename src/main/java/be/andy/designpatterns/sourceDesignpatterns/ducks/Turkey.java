package be.andy.designpatterns.sourceDesignpatterns.ducks;

public interface Turkey {
	public void gobble();
	public void fly();
}
