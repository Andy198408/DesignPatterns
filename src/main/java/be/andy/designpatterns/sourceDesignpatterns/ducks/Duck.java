package be.andy.designpatterns.sourceDesignpatterns.ducks;

public interface Duck {
	public void quack();
	public void fly();
}
