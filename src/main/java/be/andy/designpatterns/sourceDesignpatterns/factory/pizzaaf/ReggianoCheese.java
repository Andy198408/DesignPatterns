package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class ReggianoCheese implements Cheese {

	public String toString() {
		return "Reggiano Cheese";
	}
}
