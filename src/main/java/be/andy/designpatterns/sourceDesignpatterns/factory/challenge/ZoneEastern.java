package be.andy.designpatterns.sourceDesignpatterns.factory.challenge;

public class ZoneEastern extends Zone {
	public ZoneEastern() {
		displayName = "US/Eastern";
		offset = -5;
	}
}