package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class MozzarellaCheese implements Cheese {

	public String toString() {
		return "Shredded Mozzarella";
	}
}
