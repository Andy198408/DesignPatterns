package be.andy.designpatterns.sourceDesignpatterns.factory.challenge;

public class ZoneMountain extends Zone {
	public ZoneMountain() {
		displayName = "US/Mountain";
		offset = -7;
	}
}