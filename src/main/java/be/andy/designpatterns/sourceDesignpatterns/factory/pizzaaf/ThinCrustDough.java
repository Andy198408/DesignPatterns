package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class ThinCrustDough implements Dough {
	public String toString() {
		return "Thin Crust Dough";
	}
}
