package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class BlackOlives implements Veggies {

	public String toString() {
		return "Black Olives";
	}
}
