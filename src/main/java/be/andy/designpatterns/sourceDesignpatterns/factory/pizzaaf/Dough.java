package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public interface Dough {
	public String toString();
}
