package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class Eggplant implements Veggies {

	public String toString() {
		return "Eggplant";
	}
}
