package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class ThickCrustDough implements Dough {
	public String toString() {
		return "ThickCrust style extra thick crust dough";
	}
}
