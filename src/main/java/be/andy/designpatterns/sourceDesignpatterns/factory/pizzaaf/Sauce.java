package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public interface Sauce {
	public String toString();
}
