package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public interface Cheese {
	public String toString();
}
