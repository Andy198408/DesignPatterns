package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public interface Pepperoni {
	public String toString();
}
