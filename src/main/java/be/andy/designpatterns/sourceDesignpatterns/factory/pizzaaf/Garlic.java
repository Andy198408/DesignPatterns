package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class Garlic implements Veggies {

	public String toString() {
		return "Garlic";
	}
}
