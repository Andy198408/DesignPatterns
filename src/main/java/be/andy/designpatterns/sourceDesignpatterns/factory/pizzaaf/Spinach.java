package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class Spinach implements Veggies {

	public String toString() {
		return "Spinach";
	}
}
