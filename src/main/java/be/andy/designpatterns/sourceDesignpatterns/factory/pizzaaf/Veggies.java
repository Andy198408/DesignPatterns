package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public interface Veggies {
	public String toString();
}
