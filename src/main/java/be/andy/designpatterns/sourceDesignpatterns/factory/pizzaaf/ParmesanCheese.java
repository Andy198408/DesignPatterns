package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class ParmesanCheese implements Cheese {

	public String toString() {
		return "Shredded Parmesan";
	}
}
