package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class Onion implements Veggies {

	public String toString() {
		return "Onion";
	}
}
