package be.andy.designpatterns.sourceDesignpatterns.factory.pizzaaf;

public class SlicedPepperoni implements Pepperoni {

	public String toString() {
		return "Sliced Pepperoni";
	}
}
