package be.andy.designpatterns.sourceDesignpatterns.singleton.subclass;

public class HotterSingleton extends Singleton {
	// useful instance variables here
 
	private HotterSingleton() {
		super();
	}
 
	// useful methods here
}
