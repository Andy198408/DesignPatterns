package be.andy.designpatterns.sourceDesignpatterns.command.party;

public interface Command {
	public void execute();
	public void undo();
}
