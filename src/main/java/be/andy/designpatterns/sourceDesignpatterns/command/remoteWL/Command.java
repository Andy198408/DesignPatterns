package be.andy.designpatterns.sourceDesignpatterns.command.remoteWL;

@FunctionalInterface
public interface Command {
	public void execute();
}
