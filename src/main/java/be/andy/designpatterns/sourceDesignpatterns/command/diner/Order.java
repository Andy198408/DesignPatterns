package be.andy.designpatterns.sourceDesignpatterns.command.diner;

@FunctionalInterface
public interface Order {
	public void orderUp();
}
