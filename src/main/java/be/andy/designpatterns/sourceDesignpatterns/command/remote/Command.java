package be.andy.designpatterns.sourceDesignpatterns.command.remote;

public interface Command {
	public void execute();
}
