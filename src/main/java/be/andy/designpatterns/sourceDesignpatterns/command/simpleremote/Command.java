package be.andy.designpatterns.sourceDesignpatterns.command.simpleremote;

public interface Command {
	public void execute();
}
