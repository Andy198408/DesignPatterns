package be.andy.designpatterns.sourceDesignpatterns.command.simpleremoteWL;

@FunctionalInterface 
public interface Command {
	public void execute();
}
