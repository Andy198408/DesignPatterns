package be.andy.designpatterns.sourceDesignpatterns.command.undo;

public class NoCommand implements Command {
	public void execute() { }
	public void undo() { }
}
