package be.andy.designpatterns.sourceDesignpatterns.command.dinerLambda;

@FunctionalInterface
public interface Order {
	public void orderUp();
}
