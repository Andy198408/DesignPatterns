package be.andy.designpatterns.sourceDesignpatterns.command.remote;

public class NoCommand implements Command {
	public void execute() { }
}
