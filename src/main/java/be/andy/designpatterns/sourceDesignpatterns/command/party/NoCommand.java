package be.andy.designpatterns.sourceDesignpatterns.command.party;

public class NoCommand implements Command {
	public void execute() { }
	public void undo() { }
}
