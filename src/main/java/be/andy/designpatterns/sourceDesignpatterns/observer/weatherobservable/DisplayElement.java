package be.andy.designpatterns.sourceDesignpatterns.observer.weatherobservable;

public interface DisplayElement {
	public void display();
}
