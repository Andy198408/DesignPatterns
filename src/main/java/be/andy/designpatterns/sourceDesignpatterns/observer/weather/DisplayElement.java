package be.andy.designpatterns.sourceDesignpatterns.observer.weather;

public interface DisplayElement {
	public void display();
}
