package be.andy.designpatterns.sourceDesignpatterns.observer.weather;

public interface Observer {
	public void update(float temp, float humidity, float pressure);
}
