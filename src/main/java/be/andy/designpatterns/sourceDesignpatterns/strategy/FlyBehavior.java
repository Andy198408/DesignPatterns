package be.andy.designpatterns.sourceDesignpatterns.strategy;

public interface FlyBehavior {
	public void fly();
}
