package be.andy.designpatterns.sourceDesignpatterns.strategy.challenge;

@FunctionalInterface
public interface ShareStrategy {
	public void share();
}
