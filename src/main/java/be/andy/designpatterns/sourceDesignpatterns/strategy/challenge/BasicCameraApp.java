package be.andy.designpatterns.sourceDesignpatterns.strategy.challenge;

public class BasicCameraApp extends PhoneCameraApp {
	public void edit() {
		System.out.println("Basic editing features");
	}
}
