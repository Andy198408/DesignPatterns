package be.andy.designpatterns.sourceDesignpatterns.strategy;

public interface QuackBehavior {
	public void quack();
}
