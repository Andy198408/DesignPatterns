package be.andy.designpatterns.sourceDesignpatterns.adapter.ducks;

public interface Turkey {
	public void gobble();
	public void fly();
}
