package be.andy.designpatterns.sourceDesignpatterns.adapter.ducks;

public interface Duck {
	public void quack();
	public void fly();
}
