package be.andy.designpatterns.sourceDesignpatterns.collections.iterator_builtin;

import java.util.Iterator;

public interface Menu {
	public Iterator<String> createIterator();
}
