package be.andy.designpatterns.sourceDesignpatterns.collections.iterator;

public interface Iterator {
	boolean hasNext();
	Object next();
}
