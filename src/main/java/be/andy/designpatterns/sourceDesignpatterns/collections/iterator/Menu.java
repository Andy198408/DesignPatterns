package be.andy.designpatterns.sourceDesignpatterns.collections.iterator;

public interface Menu {
	public Iterator createIterator();
}
