package be.andy.designpatterns.sourceDesignpatterns.iterator.dinermerger;

public interface Menu {
	public Iterator createIterator();
}
