package be.andy.designpatterns.sourceDesignpatterns.iterator.dinermerger;

public interface Iterator {
	boolean hasNext();
	MenuItem next();
}
