package be.andy.designpatterns.sourceDesignpatterns.decorator.starbuzzWithSizes;

public class Decaf extends Beverage {
	public Decaf() {
		description = "Decaf Coffee";
	}
 
	public double cost() {
		return 1.05;
	}
}

