package be.andy.designpatterns.DecoratorPattern.IoInputstreamExample;

import org.junit.jupiter.api.Test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

class LowerCaseInputStreamTest {

    private static String TESTFILE = "D:\\coding\\courses\\Head-First-Design-Patterns-master\\test.txt";

    @Test
    void testStream() {
        int c;
        try {
            InputStream in =
                    new LowerCaseInputStream(
                            new BufferedInputStream(
                                    new FileInputStream(TESTFILE)));
            while ((c = in.read()) >= 0) {
                System.out.print((char) c);
//                System.out.println(c);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}