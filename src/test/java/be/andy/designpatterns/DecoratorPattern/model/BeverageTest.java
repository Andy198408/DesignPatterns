package be.andy.designpatterns.DecoratorPattern.model;

import be.andy.designpatterns.DecoratorPattern.beverage.*;
import org.junit.jupiter.api.Test;


class BeverageTest {
    @Test
    void makeStarbuzzCoffee() {
        Beverage beverage = new Espresso();
        System.out.println(beverage.getDescription()
                + " $" + beverage.cost());
        Beverage beverage2 = new DarkRoast();
        beverage2 = new Mocha(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println(beverage2.getDescription()
                + " $" + beverage2.cost());
        Beverage beverage3 = new HouseBlend();
        beverage3 = new Soy(beverage3);
        beverage3 = new Whip(beverage3);
        beverage3 = new Mocha(beverage3);

        System.out.println(beverage3.getDescription()
                + " $" + beverage3.cost());

        System.out.println("Print list");
        beverage3.printList();

        System.out.println("Test runnable");
        Beverage finalBeverage = beverage3;
        Runnable runnable1 = () -> finalBeverage.getDescription();  // doesn't work
        runnable1.run();

        DoMore doMore = (DoMore) beverage3; // loses all beverage functionality
        System.out.println("Test do more stuff");
        doMore.doMoreStuff();
        System.out.println("Test getdescription domore");
        System.out.println(doMore.getDescription());
        System.out.println("Test getdescription beverage3");
        System.out.println(beverage3.getDescription());
        System.out.println("Test runnable");
        Runnable runnable = () -> doMore.doMoreStuff();
        runnable.run();
    }

    @Test
    void beverageSize() {
        Beverage beverage3 = new HouseBlend();
        beverage3.setSize(Beverage.Size.VENTI);
        beverage3 = new Soy(beverage3);
        beverage3 = new Mocha(beverage3);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription()
                + " $" + beverage3.cost());

        beverage3 = new HouseBlend();
        beverage3.setSize(Beverage.Size.TALL);
        beverage3 = new Soy(beverage3);
        beverage3 = new Mocha(beverage3);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription()
                + " $" + beverage3.cost());

        beverage3 = new HouseBlend();
        beverage3.setSize(Beverage.Size.GRANDE);
        beverage3 = new Soy(beverage3);
        beverage3 = new Mocha(beverage3);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription()
                + " $" + beverage3.cost());
    }
}