package be.andy.designpatterns.commandPattern.simpleRemote;

import be.andy.designpatterns.commandPattern.command.GarageDoorOpenCommand;
import be.andy.designpatterns.commandPattern.command.LightOnCommand;
import be.andy.designpatterns.commandPattern.model.GarageDoor;
import be.andy.designpatterns.commandPattern.model.Light;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SimpleRemoteControlArrayTest {

    SimpleRemoteControl remote;

    @BeforeEach
    void setUp() {
        remote = new SimpleRemoteControl();
    }

    @Test
    void setCommandLight() {
        Light light = new Light();
        LightOnCommand lightOn = new LightOnCommand(light);
        remote.setCommand(lightOn);
        remote.buttonWasPressed();
    }

    @Test
    void setCommandGarageDoor() {
        Light light = new Light();
        GarageDoor garageDoor = new GarageDoor();

        LightOnCommand lightOnCommand = new LightOnCommand(light);
        GarageDoorOpenCommand garageDoorOpenCommand = new GarageDoorOpenCommand(garageDoor);

        remote.setCommand(lightOnCommand);
        remote.buttonWasPressed();
        remote.setCommand(garageDoorOpenCommand);
        remote.buttonWasPressed();
    }
}