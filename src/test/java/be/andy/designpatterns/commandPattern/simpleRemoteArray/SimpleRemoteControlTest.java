package be.andy.designpatterns.commandPattern.simpleRemoteArray;

import be.andy.designpatterns.commandPattern.command.CeilingFanOffCommand;
import be.andy.designpatterns.commandPattern.command.CeilingFanOnCommand;
import be.andy.designpatterns.commandPattern.command.GarageDoorDownCommand;
import be.andy.designpatterns.commandPattern.command.GarageDoorUpCommand;
import be.andy.designpatterns.commandPattern.command.LightOffCommand;
import be.andy.designpatterns.commandPattern.command.LightOnCommand;
import be.andy.designpatterns.commandPattern.command.StereoOffCommand;
import be.andy.designpatterns.commandPattern.command.StereoOnWithCDCommand;
import be.andy.designpatterns.commandPattern.model.CeilingFan;
import be.andy.designpatterns.commandPattern.model.GarageDoor;
import be.andy.designpatterns.commandPattern.model.Light;
import be.andy.designpatterns.commandPattern.model.Stereo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SimpleRemoteControlTest {

    SimpleRemoteControlArray remoteControl;

    @BeforeEach
    void setUp() {
        remoteControl = new SimpleRemoteControlArray();
    }

    @Test
    void testRemote() {
        Light livingRoomLight = new Light("Living Room");
        Light kitchenLight = new Light("Kitchen");
        CeilingFan ceilingFan = new CeilingFan("Living Room");
        GarageDoor garageDoor = new GarageDoor("Garage");
        Stereo stereo = new Stereo("Living Room");
        LightOnCommand livingRoomLightOn =
                new LightOnCommand(livingRoomLight);
        LightOffCommand livingRoomLightOff =
                new LightOffCommand(livingRoomLight);
        LightOnCommand kitchenLightOn =
                new LightOnCommand(kitchenLight);
        LightOffCommand kitchenLightOff =
                new LightOffCommand(kitchenLight);
        CeilingFanOnCommand ceilingFanOn =
                new CeilingFanOnCommand(ceilingFan);
        CeilingFanOffCommand ceilingFanOff =
                new CeilingFanOffCommand(ceilingFan);
        GarageDoorUpCommand garageDoorUp =
                new GarageDoorUpCommand(garageDoor);
        GarageDoorDownCommand garageDoorDown =
                new GarageDoorDownCommand(garageDoor);
        StereoOnWithCDCommand stereoOnWithCD =
                new StereoOnWithCDCommand(stereo);
        StereoOffCommand stereoOff =
                new StereoOffCommand(stereo);
        remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff);
        remoteControl.setCommand(1, kitchenLightOn, kitchenLightOff);
        remoteControl.setCommand(2, ceilingFanOn, ceilingFanOff);
        remoteControl.setCommand(3, stereoOnWithCD, stereoOff);
        System.out.println(remoteControl);
        remoteControl.onButtonWasPushed(0);
        remoteControl.offButtonWasPushed(0);
        remoteControl.onButtonWasPushed(1);
        remoteControl.offButtonWasPushed(1);
        remoteControl.onButtonWasPushed(2);
        remoteControl.offButtonWasPushed(2);
        remoteControl.onButtonWasPushed(3);
        remoteControl.offButtonWasPushed(3);
    }

    @Test
    void testRemoteLambda() {
        Light livingRoomLight = new Light("Living Room");
        Light kitchenLight = new Light("Kitchen");
        CeilingFan ceilingFan = new CeilingFan("Living Room");
        GarageDoor garageDoor = new GarageDoor("Garage");
        Stereo stereo = new Stereo("Living Room");

        remoteControl.setCommand(0, () -> livingRoomLight.on(), () -> livingRoomLight.off());
        remoteControl.setCommand(1, () -> kitchenLight.on(), () -> kitchenLight.off());
        remoteControl.setCommand(2, () -> ceilingFan.high(), () -> ceilingFan.off());
        remoteControl.setCommand(3, () -> {
            stereo.on();
            stereo.setCD();
            stereo.setVolume(11);
        }, () -> stereo.off());
        System.out.println(remoteControl);
        remoteControl.onButtonWasPushed(0);
        remoteControl.offButtonWasPushed(0);
        remoteControl.onButtonWasPushed(1);
        remoteControl.offButtonWasPushed(1);
        remoteControl.onButtonWasPushed(2);
        remoteControl.offButtonWasPushed(2);
        remoteControl.onButtonWasPushed(3);
        remoteControl.offButtonWasPushed(3);
    }
}