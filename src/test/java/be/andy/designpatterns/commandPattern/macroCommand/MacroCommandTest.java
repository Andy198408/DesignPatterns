package be.andy.designpatterns.commandPattern.macroCommand;

import be.andy.designpatterns.commandPattern.command.*;
import be.andy.designpatterns.commandPattern.model.*;
import be.andy.designpatterns.commandPattern.simpleRemoteArray.SimpleRemoteControlArray;
import be.andy.designpatterns.commandPattern.simpleRemoteArray.SimpleRemoteControlArrayUndo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MacroCommandTest {

    SimpleRemoteControlArray remote;
    SimpleRemoteControlArrayUndo remoteUndo;

    @BeforeEach
    void setUp() {
        remote = new SimpleRemoteControlArray();
        remoteUndo = new SimpleRemoteControlArrayUndo();
    }

    @Test
    void testMacroCommand() {
        Light light = new Light("Living room");
        TV tv = new TV("Living room");
        Stereo stereo = new Stereo("Stereo");
        Hottub hottub = new Hottub();

        LightOnCommand lightOnCommand = new LightOnCommand(light);
        StereoOnWithCDCommand stereoOnWithCDCommand = new StereoOnWithCDCommand(stereo);
        TVOnCommand tvOnCommand = new TVOnCommand(tv);
        HottubOnCommand hottubOnCommand = new HottubOnCommand(hottub);

        LightOffCommand lightOffCommand = new LightOffCommand(light);
        StereoOffCommand stereoOffCommand = new StereoOffCommand(stereo);
        TVOffCommand tvOffCommand = new TVOffCommand(tv);
        HottubOffCommand hottubOffCommand = new HottubOffCommand(hottub);

        Command[] partyOn = {lightOnCommand, stereoOnWithCDCommand, tvOnCommand, hottubOnCommand};
        Command[] partyOff = {lightOffCommand, stereoOffCommand, tvOffCommand, hottubOffCommand};

        MacroCommand partyOnMacro = new MacroCommand(partyOn);
        MacroCommand partyOffMacro = new MacroCommand(partyOff);

        remote.setCommand(0, partyOnMacro, partyOffMacro);

        System.out.print(remote);
        remote.onButtonWasPushed(0);
        remote.offButtonWasPushed(0);
    }

    @Test
    void testMacroCommandUndo() {
        Light light = new Light("Living room");
        Stereo stereo = new Stereo("Stereo");
        Hottub hottub = new Hottub();

        be.andy.designpatterns.commandPattern.commandUndo.LightOnCommand lightOnCommand = new be.andy.designpatterns.commandPattern.commandUndo.LightOnCommand(light);
        be.andy.designpatterns.commandPattern.commandUndo.StereoOnWithCDCommand stereoOnWithCDCommand = new be.andy.designpatterns.commandPattern.commandUndo.StereoOnWithCDCommand(stereo);
        be.andy.designpatterns.commandPattern.commandUndo.HottubOnCommand hottubOnCommand = new be.andy.designpatterns.commandPattern.commandUndo.HottubOnCommand(hottub);

        be.andy.designpatterns.commandPattern.commandUndo.LightOffCommand lightOffCommand = new be.andy.designpatterns.commandPattern.commandUndo.LightOffCommand(light);
        be.andy.designpatterns.commandPattern.commandUndo.StereoOffCommand stereoOffCommand = new be.andy.designpatterns.commandPattern.commandUndo.StereoOffCommand(stereo);
        be.andy.designpatterns.commandPattern.commandUndo.HottubOffCommand hottubOffCommand = new be.andy.designpatterns.commandPattern.commandUndo.HottubOffCommand(hottub);

        CommandUndo[] partyOn = {lightOnCommand, stereoOnWithCDCommand, hottubOnCommand};
        CommandUndo[] partyOff = {lightOffCommand, stereoOffCommand, hottubOffCommand};

        MacroCommandUndo partyOnMacro = new MacroCommandUndo(partyOn);
        MacroCommandUndo partyOffMacro = new MacroCommandUndo(partyOff);

        remoteUndo.setCommand(0, partyOnMacro, partyOffMacro);

        System.out.print(remoteUndo);
        remoteUndo.onButtonWasPushed(0);
        remoteUndo.undoButtonWasPushed();

    }
}