package be.andy.designpatterns.singletonPattern.chocolateFactory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChocolateBoilerEnumTest {

    @Test
    void BoilerEnumTest() {
        ChocolateBoilerEnum instance = ChocolateBoilerEnum.INSTANCE;
        System.out.println(instance.isEmpty());
        instance.fill();
        System.out.println(instance.isEmpty());
    }

    @Test
    void BoilerEnumSamInstanceTest() {
        ChocolateBoilerEnum instance = ChocolateBoilerEnum.INSTANCE;
        System.out.println(instance.isEmpty());
        instance.fill();
        System.out.println(instance.isEmpty());
    }


}