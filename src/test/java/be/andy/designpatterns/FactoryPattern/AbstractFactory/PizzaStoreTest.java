package be.andy.designpatterns.FactoryPattern.AbstractFactory;

import org.junit.jupiter.api.Test;


class PizzaStoreTest {
    @Test
    void orderPizza() {
        PizzaStore nyPizzaStore = new NYPizzaStore();
        Pizza pizza = nyPizzaStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza.getName() + "\n");

        ChicagoPizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
        pizza = chicagoPizzaStore.orderPizza("cheese");
        System.out.println("Joel ordered a " + pizza.getName() + "\n");
    }
}