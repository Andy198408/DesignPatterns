package be.andy.designpatterns.adapterPattern.iteratorEnumeration;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IteratorEnumerationTest {
    @Test
    void iteratorEnumerationtest() {
        List<Integer> integers = new ArrayList<>(Arrays.asList(1,2,3,4));
        Iterator<Integer> iterator = integers.iterator();
        Enumeration iteratorEnumeration = new IteratorEnumeration(iterator);

        iterator.forEachRemaining(System.out::println);

        assertFalse(iteratorEnumeration.hasMoreElements());
        integers.add(6);
        assertTrue(iteratorEnumeration.hasMoreElements());

    }
}