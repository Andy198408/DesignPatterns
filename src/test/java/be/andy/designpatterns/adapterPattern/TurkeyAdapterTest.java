package be.andy.designpatterns.adapterPattern;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class TurkeyAdapterTest {
    @Test
    void adapterTest() {
        MallardDuck mallardDuck = new MallardDuck();
        WildTurkey wildTurkey = new WildTurkey();

        Duck turkeyAdapter = new TurkeyAdapter(wildTurkey);

        System.out.println("The Turkey says...");
        wildTurkey.gobble();
        wildTurkey.fly();

        System.out.println("\nThe Duck says...");
        testDuck(mallardDuck);
        System.out.println("\nThe TurkeyAdapter says...");
        testDuck(turkeyAdapter);


    }

    static void testDuck(Duck duck) {
        duck.quack();
        duck.fly();
    }

    public static List<String> readAllLinesInFile(String fileName)
    {
        List<String> lines = Collections.emptyList();
        try
        {
            lines =
                Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return lines;
    }

    @Test
    void name() {
        int[] test = new int[]{5, 4, 6, 7, 8, 9, 6, 3};

        bubbleSort(test);

        for (int i : test) {
            System.out.println(i);
        }

        bubbleSortReverse(test);
        Arrays.stream(test).forEach(System.out::println);

    }

    public void bubbleSort(int[] arr) {
        // get the last index of the array
        var lastIndex = arr.length - 1;

        // outer loop to traverse through all the elements of the array
        for (var j = 0; j < lastIndex; j++) {
            // inner loop to traverse the unsorted part of the array
            for (var i = 0; i < lastIndex - j; i++) {
                // swap the elements if they are in the wrong order
                if (arr[i] > arr[i + 1]) {
                    var tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                }
            }
        }
    }

    public void bubbleSortReverse(int[] arr) {
        int lastIndex = arr.length - 1;

        for (int j = 0; j < lastIndex; j++) {
            for (int i = 0; i < lastIndex - j; i++) {
                if (arr[i] < arr[i + 1]) {
                    var tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                }
            }
        }
    }
}
//5, 4, 6, 7, 8, 9, 6, 3
//
//4 5 6 7 8 6 3 9
//
//4 5 6 7 8 3 6 | 9
//
//4 5 6 7 3 6 | 8 9
//
//4 5 6 3 6 | 7 8 9
