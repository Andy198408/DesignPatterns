package be.andy.designpatterns.templateMethodPattern;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class BeverageTestDrive {
    public static void main(String[] args) {
        CaffeineBeverageWithHook teaWithHook = new TeaWithHook();
        CaffeineBeverageWithHook coffeeWithHook = new CoffeeWithHook();

        teaWithHook.test = "ab";
        coffeeWithHook.test = "bb";

        List<CaffeineBeverageWithHook> arr = Arrays.asList(coffeeWithHook, teaWithHook);
        arr.forEach(System.out::println);
        arr.sort(CaffeineBeverageWithHook::compareTo);
        Arrays.sort(arr.toArray());
        arr.forEach(System.out::println);

        System.out.println("Making Coffee");
        coffeeWithHook.prepareRecipe();

        System.out.println("Making Tea");
        teaWithHook.prepareRecipe();
    }
}