package be.andy.designpatterns.StrategyPattern.Model;

import be.andy.designpatterns.StrategyPattern.Implementations.FlyRocketPowered;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DuckTest {
    @Test
    void testDucks() {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();

        DuckCallQuack duckCallQuack = new DuckCallQuack();
        duckCallQuack.quack();
    }
}